FROM node:14.15.0-alpine3.12
LABEL authors="mohsennoori"

#WORKDIR /usr/src/app

#RUN npm cache clean
#RUN rm -rf ./node_modules

COPY package*.json .
RUN npm install

COPY . .

EXPOSE 3000

RUN npm build

CMD ["npm", "start"]
